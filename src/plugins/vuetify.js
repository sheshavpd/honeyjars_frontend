import Vue from "vue";
import Vuetify from "vuetify/lib/framework";

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: '#ffcc4d',
        secondary: '#484848',
        accent: '#ce9e1d',
        error: '#b71c1c',
      },
    },
  },
});

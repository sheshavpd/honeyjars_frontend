import {
  addRemote,
  getAllRemotes,
  stopContainer,
  deleteContainer,
  startContainer, createContainer
} from "../../api/remotes";

const state = {
  allRemotes: [],
};

const mutations = {
  SET_REMOTES(state, remotes) {
    state.allRemotes = remotes;
  },
};

const actions = {
  async initialize({ dispatch }) {
    const tryFetch = async () => {
      dispatch("loadRemotes");
    };
    await tryFetch();
    setInterval(tryFetch, 2000);
  },

  async loadRemotes({ commit }) {
    try {
      const remotes = (await getAllRemotes()).allRemotes;
      commit("SET_REMOTES", remotes);
    } catch (e) {
      console.log(e);
    }
  },

  async addRemote({ dispatch }, { host, port }) {
    try {
      const res = await addRemote(host, port);
      dispatch("loadRemotes");
      if (res.error) {
        console.error(res.message);
        return false;
      }
    } catch (e) {
      console.log(e);
      return false;
    }
    return true;
  },

  async stopContainer({ dispatch }, { host, port, Id }) {
    try {
      const res = await stopContainer(host, port, Id);
      dispatch("loadRemotes");
      if (res.error) {
        console.error(res.message);
        return false;
      }
    } catch (e) {
      console.log(e);
      return false;
    }
    return true;
  },

  async startContainer({ dispatch }, { host, port, Id }) {
    try {
      const res = await startContainer(host, port, Id);
      dispatch("loadRemotes");
      if (res.error) {
        console.error(res.message);
        return false;
      }
    } catch (e) {
      console.log(e);
      return false;
    }
    return true;
  },

  async deleteContainer({ dispatch }, { host, port, Id }) {
    try {
      const res = await deleteContainer(host, port, Id);
      dispatch("loadRemotes");
      if (res.error) {
        console.error(res.message);
        return false;
      }
    } catch (e) {
      console.log(e);
      return false;
    }
    return true;
  },

  async createContainer({ dispatch }, reqData) {
    try {
      const res = await createContainer(reqData);
      dispatch("loadRemotes");
      if (res.error) {
        console.error(res.message);
        return false;
      }
    } catch (e) {
      console.log(e);
      return false;
    }
    return true;
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};

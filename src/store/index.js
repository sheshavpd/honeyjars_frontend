import Vue from "vue";
import Vuex from "vuex";
import remotes from "./modules/remotes";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    remotes
  },
});

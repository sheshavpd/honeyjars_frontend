import axios from "axios";

export async function getAllRemotes() {
  return (await axios.get("/remotes/all")).data;
}

export async function addRemote(host, port) {
  return (
    await axios.post("/remotes/add", {
      host,
      port,
    })
  ).data;
}

export async function createContainer(reqData) {
  return (await axios.post("/docker/services/create", reqData)).data;
}

export async function stopContainer(host, port, Id) {
  return (
    await axios.post("/docker/services/stop", {
      host,
      port,
      Id,
    })
  ).data;
}

export async function deleteContainer(host, port, Id) {
  return (
    await axios.post("/docker/services/delete", {
      host,
      port,
      Id,
    })
  ).data;
}

export async function startContainer(host, port, Id) {
  return (
    await axios.post("/docker/services/start", {
      host,
      port,
      Id,
    })
  ).data;
}

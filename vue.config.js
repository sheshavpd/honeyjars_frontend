module.exports = {
  pages: {
    index: {
      entry: "src/main.js",
      title: "HoneyJars",
    },
  },
  transpileDependencies: ["vuetify"],
};
